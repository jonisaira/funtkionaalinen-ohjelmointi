'use strict';

//let f, g;
function foo() {
  let x = 1;
  return{
      kasvata: function() { return ++x; },
      vahenna: function() { return --x; }
  }
}

var Moduuli = (foo())
console.log(Moduuli.kasvata())
console.log(Moduuli.vahenna())
console.log(Moduuli.vahenna())