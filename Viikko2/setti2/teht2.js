var Immutable = require("immutable")

const Auto = (function(){
    const suojatut = new WeakMap();
    
    class Auto{
        constructor(tankki, matkamittari){
            suojatut.set(this, {matkamittari: matkamittari})
            this.tankki = tankki
        }
        
        getMatkamittari() {return suojatut.get(this).matkamittari}
        getTankki(){return this.tankki}
        tankkaa(){this.tankki = this.tankki + 1}
        aja(){
            this.tankki = this.tankki-1;
            suojatut.set(this, {matkamittari: this.getMatkamittari()+1})
        }
    }
    return Auto
})();

const auto1 = new Auto(25, 100)
console.log("Auto luotu", auto1.getTankki(),auto1.getMatkamittari())
auto1.aja()
console.log("Autolla ajettu",auto1.getTankki(), auto1.getMatkamittari())
auto1.tankkaa()
console.log("Auto tankattu", auto1.getTankki(), auto1.getMatkamittari())
auto1.tankki = auto1.tankki+1
console.log("Auton tankkia tankattu laittomasti :(", auto1.getTankki(), auto1.getMatkamittari())