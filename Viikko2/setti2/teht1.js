let maki = (kPiste, lisapisteet) =>
    (pituus) =>{
        let pisteet  = 60;
        let lopullisetPisteet;
        //kPiste[0] = keskipisteen alku, kPiste[1] = keskipisteen loppu
        if(pituus < kPiste[0]){
            lopullisetPisteet = pisteet - lisapisteet*(kPiste[0] - pituus)
        }else if(pituus > kPiste[1]){

            lopullisetPisteet = pisteet + lisapisteet * (pituus-kPiste[1])
        }else{
            lopullisetPisteet = pisteet
        }
        return lopullisetPisteet
    } 
         
let normaaliMaki = maki([75,99], 2.0)
let suuriMaki = maki([100,130], 1.8)

let pisteet = normaaliMaki(74)
console.log(pisteet) // Alle kPisteen
pisteet = suuriMaki(168) 
console.log(pisteet) //Yli kPisteen
pisteet = suuriMaki(115) 
console.log(pisteet) //kPisteessä