import java.lang.Math;
public class teht1{
    
    public static void main(String[] args){
        
        toCelcius celcius = (double fahrenheit) -> ((double) 5/9) * (fahrenheit-32);
        area area = (double radius) -> (Math.PI * radius * radius);
        
        System.out.println(celcius.calculate(1000.00));
        System.out.println(area.calculate(5));
        
    }


    interface toCelcius{
        double calculate(double fahrenheit);
    }
    interface area{
        double calculate(double radius);
    }
}
