/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.awt.Point;
import java.util.Arrays;
import java.util.List;
import static java.util.stream.Collectors.toList;

/**
 *
 * @author joni
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {     
         List<Point> points = Arrays.asList(new
            Point(5,5), new Point(10,5));
        List<Point> newList = moveAllPointsRightBy(points, 10);
        System.out.println(newList);
        

    }
    public static List<Point> moveAllPointsRightBy(
List<Point> points, int x) {
    return
    points.stream()
    .map(p -> moveRightBy(p,x))
    .collect(toList());
    }
public static Point moveRightBy(Point p,int x){
    p.translate(x, 0);
    return p;
}
    

}
    
    

