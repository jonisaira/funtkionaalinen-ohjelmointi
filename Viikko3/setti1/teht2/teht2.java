
import java.util.*;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;
public class teht2{
    
    public static void main(String[] args){
        Trader raoul = new Trader("Raoul", "Cambridge");
        Trader mario = new Trader("Mario","Milan");
        Trader alan = new Trader("Alan","Cambridge");
        Trader brian = new Trader("Brian","Cambridge");
        List<Transaction> transactions = Arrays.asList(
            new Transaction(brian, 2011, 300), 
            new Transaction(raoul, 2012, 1000),
            new Transaction(raoul, 2011, 400),
            new Transaction(mario, 2012, 710),	
            new Transaction(mario, 2012, 700),
            new Transaction(alan, 2012, 950)
        );
        
        List<Transaction> tr2012 = transactions.stream()
                                    .filter(transaction -> transaction.getYear() > 2011 && transaction.getValue() >= 900)
                                    .collect(toList());
        //Printing traders from tr2012
        String traderStr = tr2012.stream()
            .map(transaction -> transaction.getTrader().getName())
            .reduce("", (n1, n2) -> n1+"\n"+n2);
        System.out.println(traderStr);
        
        // :(
        /*List<Dish> menu = Dish.menu.stream()
            .map(dish -> dish.getType())
            .collect(toList())
            .stream()
            .reduce(0, (n1) ->menu.indexOf(n1) > 0);
        */
    }
}