const onPalindromi = (merkkijono)=>{
    if(merkkijono.length <= 1){
        return true
    }else if(merkkijono[0] !== merkkijono[merkkijono.length-1]){
        return false
    }else{
        let temp = merkkijono.slice(1, merkkijono.length-1)
        return onPalindromi(temp)
    }
    
}
const palindromi = "saippuakauppias"
const test = onPalindromi(palindromi)
console.log(test)