function vertaaLukuja(){
    return (a,b) => {
        if( a > b){
            return 1
        }else if(a < b){
            return -1
        }else{
            return 0
        }
    } 
}
let test1 = vertaaLukuja()
console.log(test1(2,1))

let test2 = vertaaLukuja()
console.log(test2(1,2))

let test3 = vertaaLukuja()
console.log(test3(2,2))