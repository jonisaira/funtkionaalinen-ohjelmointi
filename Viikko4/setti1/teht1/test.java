/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.Arrays;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import java.awt.Point;
import java.util.Arrays;
import java.util.List;
import static java.util.stream.Collectors.toList;
import static javaapplication6.JavaApplication6.moveRightBy;
/**
 *
 * @author joni
 */
public class NewEmptyJUnitTest {
    
    public NewEmptyJUnitTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    public static List<Point> moveAllPointsRightBy(
    List<Point> points, int x) {
    return
    points.stream()
    .map(p -> moveRightBy(p,x))
    .collect(toList());
    }
    @Test
    public void testmoveAllPointsRightBy()
        throws Exception {
        List<Point> points = Arrays.asList(new
            Point(5,5), new Point(10,5));
            List<Point> expectedPoints =
            Arrays.asList(new Point(15,5),
            new Point(20,5));
            List<Point> newPoints = 
            moveAllPointsRightBy(points, 10);
        assertEquals(expectedPoints, newPoints);
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
