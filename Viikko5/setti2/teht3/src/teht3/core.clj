(ns teht3.core
  (:gen-class))

(defn tulostaKolmellaJaolliset
  [x]
  (loop [i 1]
        (when(< i x)
            (if(zero? (mod i 3))
                (println i))
                )
        
        (recur (+ i 1)))
  )
(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (tulostaKolmellaJaolliset 45)
  )
