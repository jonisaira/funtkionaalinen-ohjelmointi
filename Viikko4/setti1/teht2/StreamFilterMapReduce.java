import java.util.stream.IntStream;
public class StreamFilterMapReduce {
  public static void main(String[] args) {
  //  sum of the triples of even integers from 2 to 10
  System.out.printf(
    "Sum of the triples of even integers from 2 to 10 is: %d%n",
    IntStream.rangeClosed(1,10)
            .filter(x -> x%2 == 0) // 5 kertaa
            .peek(x -> System.out.println("filter debug: " +x)) 
            .map(x -> x*3) // 5 kertaa
            .peek(x -> System.out.println("Map debug: " +x)) 
            .sum());
  
  System.out.println("/////////////////////////////////////////");
  
    System.out.printf(
    "Sum of the triples of even integers from 2 to 10 is: %d%n",
    IntStream.rangeClosed(1,10)
            .map(x -> x*3) //10 kertaa
            .peek(x -> System.out.println("Map debug: " +x)) 
            .filter(x -> x%2 == 0) // 5 kertaa
            .peek(x -> System.out.println("Filter debug: " +x))
            .sum());
  }
}