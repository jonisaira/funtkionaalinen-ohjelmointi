function vertaaLukuja() {
    return (a, b) => {
        if (a > b) {
            return 1
        }
        else if (a < b) {
            return 0
        }
        else {
            return 0
        }
    }
}
function vertaaListoja(v2016, v2015, vertaaja){
    let pituus = v2016.length
    let tapaukset = 0;
    for(let i = 0; i<pituus; i++){
        tapaukset += vertaaja(v2016[i], v2015[i])
    }
    return tapaukset
}
const v2015 = [-3, -2, 1, 7, 15, 18, 21, 18, 13, 7, 2, -1]
const v2016 = [-5, -6, 3, 3, 16, 13, 16, 15, 10, 5, -2, -3]
var vertaaja = vertaaLukuja()
console.log(vertaaListoja(v2016, v2015, vertaaja))
