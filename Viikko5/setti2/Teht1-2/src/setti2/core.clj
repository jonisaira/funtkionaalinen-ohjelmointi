(ns setti2.core
  (:gen-class))

(defn kysyNumero
  []
  (def temp 0)

    (println "Syötä numero")
    (def x (read-line))
    (if(< 0 (Integer/valueOf x))
        (do 
            (if(even? (Integer/valueOf x))
              (println "Parillinen")
              (do
                (println "Pariton")
            )
          ))
  (do
    (println "Syötä positiivinen luku")
    (kysyNumero)))
    
  )
  
(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (kysyNumero)
  )
