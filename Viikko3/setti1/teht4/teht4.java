import java.util.*;
import java.util.stream.*;
import java.util.Map.Entry;
import javafx.util.Pair;
import java.util.ArrayList;
public class teht4{
    
    public static void main(String[] args){
        List<Integer> luvut1 = Arrays.asList(1,2,3);
        List<Integer> luvut2 = Arrays.asList(3,4);
        ArrayList<Pair<Integer,Integer>> pairList = new ArrayList<Pair<Integer, Integer>>();
        
        luvut1.stream()
            .forEach(luku1 ->
                luvut2.stream()
                .forEach(luku2 ->{
                    Pair<Integer, Integer> pair = new Pair <Integer, Integer>(luku1, luku2);
                    pairList.add(pair);
                }));
        for( Pair <Integer, Integer> pair : pairList ){
            System.out.println(pair.getKey() + ", " + pair.getValue());
        }
    }
}