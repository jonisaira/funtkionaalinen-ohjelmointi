var Immutable = require('immutable')

const set1 = Immutable.Set(['punainen', 'vihreä', 'keltainen']);
let set2 = set1.add('ruskea')
set2 = set2.add('ruskea')
const set3 = set2
console.log(set1, set2)
console.log(set1 === set2) //false
console.log(set3 === set2) //true